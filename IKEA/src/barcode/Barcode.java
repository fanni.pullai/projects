/*
A vonalkódnak van típusa (digitális, nyomtatott) és egy sorszáma (String). 
Minden második vonalkód digitális. A termékek a vonalkódok sorszáma alapján összehasonlíthatók.
 */
package barcode;

import java.util.Objects;

/**
 *
 * @author Fanni
 */
public class Barcode {
    
    private String no;
    private BarcodeType type;

    public Barcode(String no, BarcodeType type) {
        this.no = no;
        this.type = type;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public BarcodeType getType() {
        return type;
    }

    public void setType(BarcodeType type) {
        this.type = type;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.no);
        hash = 97 * hash + Objects.hashCode(this.type);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Barcode other = (Barcode) obj;
        if (!Objects.equals(this.no, other.no)) {
            return false;
        }
        if (this.type != other.type) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Barcode{" + "no=" + no + ", type=" + type + '}';
    }
    
    
    
    
}
