/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datahandler;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import productFactory.ProductFactory;
import productRepository.ProductRepository;
import products.Product;

/**
 *
 * @author Fanni
 */
public class CommandParser {
    
    private static final String DELIMITER = " ";
    private static final int NUMBER_OF_ARGS = 5;
    
    private final ProductRepository productRepository = new ProductRepository();
    
    public void parse(String command) {
        String[] commands = command.split(DELIMITER);
        
        if (commands.length != NUMBER_OF_ARGS) {
            if (commands.length == 2 && commands[0] == "REMOVE") {
                productRepository.remove(commands[1]);
            } else if (commands.length == 1 && commands[0] == "SAVE") {
                try {
                    report();
                    productRepository.serializationOut(productRepository.getProducts());
                } catch (IOException ex) {
                    Logger.getLogger(CommandParser.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                throw new IllegalArgumentException("Wrong number of arguments: " + command);
            }
        }
        
        Product product = ProductFactory.createProduct(commands[0], commands[1], commands[2], commands[3], commands[4]);
        
        productRepository.add(product);
    }
    
    public void report() throws IOException {
        new Report(productRepository).generateReport();
    }
    
}
