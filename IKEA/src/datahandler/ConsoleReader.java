/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datahandler;

/**
 *
 * @author Fanni
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConsoleReader {

    private static final String STOP_COMMAND = "EXIT";
    private static int counter = 0;

    public static void readConsole() {
       
        CommandParser parser = new CommandParser();

        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            
            String line;

            while ((line = br.readLine()) != null && !STOP_COMMAND.equalsIgnoreCase(line) && counter != 10) {
                counter++;
                parser.parse(line);
                
            }
        } catch (IOException ex) {
            Logger.getLogger(ConsoleReader.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {
            parser.report();
        } catch (IOException ex) {
            Logger.getLogger(ConsoleReader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
