/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datahandler;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import productRepository.ProductRepository;
import products.BeautyProducts;
import products.EntertanimentDevice;
import products.Product;
import type.ProductType;

/**
 *
 * @author Fanni
 */
public class Report {

    private static final String FILE_NAME = "Report.txt";

    private ProductRepository productRepository;

    public Report(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public void generateReport() throws IOException {
        try {
            reportNumberOfSavedProducts();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Report.class.getName()).log(Level.SEVERE, null, ex);
        }
        reportIntoFile();
    }

    public void reportNumberOfSavedProducts() throws ClassNotFoundException {
        int numberOfBeautyProducts = 0;
        int numbertOfEntertainmentProducts = 0;
        int numberOfKitchenWare = 0;

        if (productRepository.getProducts().size() == 0) {
            System.out.println("Nem lett termék elmentve.");
        } else {

            for (int i = 0; i < productRepository.getProducts().size(); i++) {

                if (productRepository.getProducts().get(i).getClass() == Class.forName("BeautyProducts")) {
                    numberOfBeautyProducts++;
                } else if (productRepository.getProducts().get(i).getClass() == Class.forName("EntertainmentDevice")) {
                    numbertOfEntertainmentProducts++;
                } else {
                    numberOfKitchenWare++;
                }
            }

            System.out.println(numberOfBeautyProducts + " szépségápolási termék lett elmentve.");
            System.out.println(numberOfKitchenWare + " konyhai termék lett elmentve.");
            System.out.println(numbertOfEntertainmentProducts + " szórakoztátis termék lette elmentve");
        }
    }
    
    public void numberOfAppliencesWithMin2PowerOns () throws ClassNotFoundException{
        int counter = 0;
         for (int i = 0; i < productRepository.getProducts().size(); i++) {
             if (productRepository.getProducts().get(i).getClass() == Class.forName("EntertainmentDevice")) {
                // EntertanimentDevice e = (EntertanimentDevice)productRepository.getProducts().get(i).getNumberOfSwichOns();
             }
         }
        
    }

    public void reportIntoFile() throws IOException {
        try (PrintWriter pw = new PrintWriter(new FileWriter(FILE_NAME))) {
            Set<Product> finalProducts = new TreeSet<>();
            finalProducts.addAll(productRepository.getProducts());
            finalProducts.forEach(pw::println);
        }
    }

}
