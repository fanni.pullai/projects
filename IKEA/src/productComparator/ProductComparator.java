/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.Comparator;
import products.Product;

/**
 *
 * @author Fanni
 */
public class ProductComparator implements Comparator<Product>{

    @Override
    public int compare(Product o1, Product o2) {
         return o1.getBarcode().getNo().compareTo(o2.getBarcode().getNo());
    }

  
    
}
