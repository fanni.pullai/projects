/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package productFactory;

import barcode.Barcode;
import barcode.BarcodeType;
import java.time.LocalDate;
import products.BeautyProducts;
import products.EntertanimentDevice;
import products.Kitchenware;
import products.Product;
import type.ProductType;

/**
 *
 * @author Fanni
 */
public class ProductFactory {
    
    public static Product createProduct(String barcode, String mainType, String manufaturer, String price, String type){
        ProductType productType = ProductType.valueOf(type);       
        Barcode productBarcode = new Barcode ("1", BarcodeType.valueOf(barcode)); 
        int productPrice = Integer.valueOf(price);
        LocalDate registrationDate = LocalDate.now();
        
        switch(mainType) {
            case "entertainment": return new EntertanimentDevice(productType, manufaturer, productBarcode, productPrice, registrationDate);
            case "kitchen": return new Kitchenware(productType, manufaturer, productBarcode, productPrice, registrationDate);
            case "beauty": return new BeautyProducts(productType, manufaturer, productBarcode, productPrice, registrationDate);
        }
        throw new IllegalArgumentException("I do not know what to do ...");
    }
    
}
