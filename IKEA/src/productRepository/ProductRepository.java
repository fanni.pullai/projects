package productRepository;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import products.Product;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Fanni
 */
public class ProductRepository implements Serializable {

    private List<Product> products = new ArrayList<>();

    public ProductRepository() {
    }

    public List<Product> getProducts() {
        return products;
    }

//    public void setProducts(List<Product> products) {
//        this.products = products;
//    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.products);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProductRepository other = (ProductRepository) obj;
        if (!Objects.equals(this.products, other.products)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Repository{" + "products=" + products + '}';
    }

    public void add(Product p) {
        products.add(p);
    }

    public void remove(String s) {
        for (int i = 0; i < products.size(); i++) {
            if (products.get(i).getBarcode().getNo().equals(s)) {
                products.remove(i);
            }
        }
    }

    public void serializationOut(List<Product> p) {

        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(new File("products.txt")))) {
            for (Product castle : products) {
                oos.writeObject(castle);
            }

//        ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("file.txt"));
//        objectOutputStream.writeObject(p); //lezárás, exceptiont itt kezeljük
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductRepository.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProductRepository.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void serializationIn() {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(new File("products.txt")))) {
            while (true) {
                System.out.println(ois.readObject());
            }

            //   System.out.println("..."); a while true  végtelen ciklus miatt sosem éri el
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductRepository.class.getName()).log(Level.SEVERE, null, ex);
        } catch (EOFException ex) {
            ; //valamit mindig irni kell a catch-be, itt a ";" jelzi, h vége.

        } catch (IOException ex) {
            Logger.getLogger(ProductRepository.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ProductRepository.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
