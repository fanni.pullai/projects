package products;

import barcode.Barcode;
import java.time.LocalDate;
import type.ProductType;

/**
 *
 * @author Fanni
 */
public class BeautyProducts extends Product {

    private int weight;

    public BeautyProducts(ProductType type, String manufacturer, Barcode barcode, int price, LocalDate registrationDate) {
        super(type, manufacturer, barcode, price, registrationDate);
    }

   

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + this.weight;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BeautyProducts other = (BeautyProducts) obj;
        if (this.weight != other.weight) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BeautyProducts{" + "weight=" + weight + '}';
    }

}
