/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package products;

import IKEAException.IKEAException;
import actions.CanBeSwitchedOn;
import barcode.Barcode;
import java.time.LocalDate;
import java.util.logging.Level;
import java.util.logging.Logger;
import type.ProductType;

/**
 *
 * @author Fanni
 */
public class EntertanimentDevice extends Product implements CanBeSwitchedOn {
    
    private int numberOfSwichOns = 5;

    public EntertanimentDevice(ProductType type, String manufacturer, Barcode barcode, int price, LocalDate registrationDate) {
        super(type, manufacturer, barcode, price, registrationDate);
    }

    public int getNumberOfSwichOns() {
        return numberOfSwichOns;
    }
      
    @Override
    public void switchOn() {
        if (numberOfSwichOns <= 5) {
            numberOfSwichOns++;
        } else {
            try {
                throw new IKEAException("Tonkrementem");
            } catch (IKEAException ex) {
                Logger.getLogger(EntertanimentDevice.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
