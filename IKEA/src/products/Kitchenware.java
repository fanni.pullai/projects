/*
. A konyhai eszközöket fel lehet emelni és be lehet kapcsolni. 
 */
package products;

import actions.CanBeLifted;
import actions.CanBeSwitchedOn;
import barcode.Barcode;
import java.time.LocalDate;
import type.ProductType;

/**
 *
 * @author Fanni
 */
public class Kitchenware extends Product implements CanBeLifted, CanBeSwitchedOn {

    public Kitchenware(ProductType type, String manufacturer, Barcode barcode, int price, LocalDate registrationDate) {
        super(type, manufacturer, barcode, price, registrationDate);
    }

    @Override
    public void lift() {
        System.out.println("Felemeltette.");
    }

    @Override
    public void switchOn() {
        System.out.println("Bekapcsoltam.");
    }

}
