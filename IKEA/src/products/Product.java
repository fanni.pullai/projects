/*
Minden műszaki cikknek van típusa (olcsó, átlagos, drága, luxus, népszerű),
gyártója (String), vonalkódja, ára (int)  és  tudjuk a regisztrálás dátumát (ami a felvétel napja).

 */
package products;

import barcode.Barcode;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
import type.ProductType;

/**
 *
 * @author Fanni
 */
public abstract class Product implements Comparable<Product>, Serializable {

    private ProductType type;
    private String manufacturer;
    private Barcode barcode;
    private int price;
    private LocalDate registrationDate;

    public Product(ProductType type, String manufacturer, Barcode barcode, int price, LocalDate registrationDate) {
        this.type = type;
        this.manufacturer = manufacturer;
        this.barcode = barcode;
        this.price = price;
        this.registrationDate = registrationDate;
    }

    public ProductType getType() {
        return type;
    }

    public void setType(ProductType type) {
        this.type = type;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public Barcode getBarcode() {
        return barcode;
    }

    public void setBarcode(Barcode barcode) {
        this.barcode = barcode;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public LocalDate getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDate registrationDate) {
        this.registrationDate = registrationDate;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 17 * hash + Objects.hashCode(this.type);
        hash = 17 * hash + Objects.hashCode(this.manufacturer);
        hash = 17 * hash + Objects.hashCode(this.barcode);
        hash = 17 * hash + this.price;
        hash = 17 * hash + Objects.hashCode(this.registrationDate);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Product other = (Product) obj;
        if (this.price != other.price) {
            return false;
        }
        if (!Objects.equals(this.manufacturer, other.manufacturer)) {
            return false;
        }
        if (this.type != other.type) {
            return false;
        }
        if (!Objects.equals(this.barcode, other.barcode)) {
            return false;
        }
        if (!Objects.equals(this.registrationDate, other.registrationDate)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Product{" + "type=" + type + ", manufacturer=" + manufacturer + ", barcode=" + barcode + ", price=" + price + ", registrationDate=" + registrationDate + '}';
    }

    @Override
    public int compareTo(Product o) {
//    int comparedBarcode = barcode.getNo().compareTo(o.barcode);
//        
//        return comparedBarcode == 0 ? name.compareTo(animal.name) : comparedBarcode;
        return 1;
    }

}
