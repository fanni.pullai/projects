/*
 olcsó, átlagos, drága, luxus, népszerű
 */
package type;

/**
 *
 * @author Fanni
 */
public enum ProductType {
    
    CHEAP,
    AVARAGE,
    EXPENSIVE,
    LUXURIOUS,
    POPULAR
    
}
