/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ikeasearch;

import com.mycompany.ikeasearch.entity.Product;
import com.mycompany.ikeasearch.repository.ProductRepository;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 *
 * @author Fanni
 */

@Configuration
@EnableJpaRepositories(basePackageClasses = ProductRepository.class)
//engedélyezzük, hogy a spring a repository-s implementációkat odaadja nekünk
@ComponentScan(basePackages = "com.mycompany.ikeasearch")
@PropertySource("classpath:application.properties")
@EntityScan(basePackageClasses = Product.class)
public class Config {
    
}
