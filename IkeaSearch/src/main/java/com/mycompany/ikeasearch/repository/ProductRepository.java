/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ikeasearch.repository;

import com.mycompany.ikeasearch.entity.Product;
import java.math.BigDecimal;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Fanni
 */
@Repository
public interface ProductRepository extends JpaRepository<Product, String> {
        
    public List<Product> findByQuantityGreaterThan(int max);

 

}
