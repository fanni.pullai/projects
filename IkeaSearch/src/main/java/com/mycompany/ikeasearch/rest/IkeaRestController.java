/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ikeasearch.rest;

import com.mycompany.ikeasearch.dto.ProductSearchResponse;
import com.mycompany.ikeasearch.service.IkeaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Fanni
 */
@RestController
@RequestMapping("ikea_search")
public class IkeaRestController {
    
    @Autowired
    private IkeaService ikeaService;

    @RequestMapping(method = RequestMethod.GET, path = "hello")
    public String hello() {
        return "Hello";
    }

    @RequestMapping(method = RequestMethod.POST, path = "filter")
    public ResponseEntity searchProducts (){ 
        ProductSearchResponse productSearchResponse = new ProductSearchResponse();
        productSearchResponse.setProducts(ikeaService.searchProducts());
        
        return ResponseEntity.ok(productSearchResponse);
    }
}