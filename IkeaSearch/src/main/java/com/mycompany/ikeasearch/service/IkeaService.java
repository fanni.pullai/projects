/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ikeasearch.service;

import com.mycompany.ikeasearch.dto.ProductDto;
import com.mycompany.ikeasearch.entity.Product;
import com.mycompany.ikeasearch.repository.ProductRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Fanni
 */
@Service
public class IkeaService {
    
    @Autowired
    private ProductRepository productRepository;

    public List<ProductDto> searchProducts() {
        

        List<ProductDto> productDtos = new ArrayList<>();
        List<Product> products = productRepository.findByQuantityGreaterThan(4);
        products.forEach(item -> {
            ProductDto dto = new ProductDto();
            BeanUtils.copyProperties(item, dto);
            productDtos.add(dto);
        });

        return productDtos;

    }

}
